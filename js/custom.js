jQuery(document).ready(function(){
jQuery('#main-slider').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        },
        3000:{
            items:1
        }
    }
});
jQuery('#games').owlCarousel({
    loop:true,
    margin:10,
	lazyLoad:true,
    responsive:{
        0:{
            items:4
        },
        600:{
            items:1
        },
        1000:{
            items:4
        },
        3000:{
            items:4
        }
    }
});	
	jQuery('.owl-nav').css('display' , 'block');
	jQuery('.owl-prev').css('background-image' , 'url("images/left-sldr.png")');
	jQuery('.owl-next').css('background-image' , 'url("images/right-sldr.png")');
jQuery(".owl-prev, .owl-next").empty();
//jQuery(".owl-next").append("<img src='images/right-sldr.png' />");
});
